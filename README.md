# E-Paper-Display picture frame

Main repository: [https://gitlab.spielmannsolutions.com/shezi/epd-pictureframe](https://gitlab.spielmannsolutions.com/shezi/epd-pictureframe)  
Gitlab mirror: [https://gitlab.com/jspielmann/epd-pictureframe](https://gitlab.com/jspielmann/epd-pictureframe)

A picture frame that retrieves a new image from the server from time to time and 
a server that serves a rotation of images.

This project consists of two parts: one `embedded`part that runs on the actual
ESP32-powered display, and one `server` part that serves the image sequences.

## Embedded

The actual display, running on an [Inkplate 6](https://inkplate.io/).

Before the display can show images, some setup is necessary; this is detailed below.


## Server

The server is a regular Django application that can serve one or many image
sequences. You need to host it somewhere where it can be seen by the display,
like in your local network or on the internet.

It should be safe to run on the internet, but no guarantee is given for that.


# Getting started

To have a changing display, you must follow several setup steps.

First, you need to set up your Django server. Follow the steps outlined
in [server/README.md](server/README.md) to prepare the basic setup. Once the server
running, you must *create a sequence* in the interface. Log in to the server to create
the sequence and upload at least one image. Note the sequence key.

Then, set up your display. Make a copy of the file
[embedded/config_example.py](embedded/config_example.py) and call it `config.py`
In your new [embedded/config.py](embedded/config.py), add your
configuration details: your wifi name and password, your server address and
sequence key from above. If you want, you can change the delay settings to your
liking.

Copy all the files to the EPD and press the reset button.

Your display should now be working and show your first image.

Upload more images to have some variety. The server will prefer newer images over
older ones, so update the sequence frequently for interesting results.


# License

Copyright 2021 Johannes Spielmann  
This file is part of epd-pictureframe.

epd-pictureframe is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

epd-pictureframe is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with epd-pictureframe.  If not, see <https://www.gnu.org/licenses/>.
