# e-Paper-Display picture frame

The actual display, running on an [Inkplate 6](https://inkplate.io/).

All our code is in `main.py`, check there for the magic ingredients.
The rest is copied from the standard library as detailed in the section below.

Before the display can show images, some setup is necessary. Make sure you have an
image server and a sequence ready.

Make a copy of the file
[embedded/config_example.py](embedded/config_example.py) and call it `config.py`
In your new [embedded/config.py](embedded/config.py), add your
configuration details: your wifi name and password, your server address and
sequence key from above. If you want, you can change the delay settings to your
liking.

Flash micropython to the controller (see below for instructions), then copy all
the files to the EPD and press the reset button. Here's the copy command:

    python pyboard.py --device /dev/ttyUSB0 -f cp inkplate.py gfx.py gfx_standard_font_01.py mcp23017.py sdcard.py config.py main.py :

Your display should now be working and show your first image.


## Flashing the firmware

Before you can start, you need to flash the firmware of the device. The firmware
is like a disk image of the software that the device is actually running. In our
case, we want the device to run [MicroPython](http://micropython.org/) and then add some of our scripts
so MicroPython can execute what we want.

So, first step, go to the download page and _choose_ one of the images. Select the one
that is in IDF4 and has SPIRAM and does not have "unstable" in its name. Make sure you
use the SPIRAM image, otherwise it won't work! At the time of writing, that means this
one:

    GENERIC-SPIRAM : esp32spiram-idf4-20210202-v1.14.bin

Download that file. For flashing that file to the controller, you need another tool called
`esptool`, which you can simply install by running

    pip install esptool
    
Please note that we need a Python 3 here. If you work in a `virtualenv` (don't worry if
you're not or if you don't know what that is), this won't be a problem. If you simply start
`python` and it gives you a version number of 2.7 or lower, try it with `python3`. If that
works, simply replace all commands with `python3` from here. If it doesn't work, you'll need
to install Python 3, either from the [official builds](https://www.python.org/downloads/),
through [homebrew](https://brew.sh/) or with [pyenv](https://github.com/pyenv/pyenv-installer).

Now we are only two commands away from having Micropython ready on your board. Connect your
Inkplate to the computer and find its connection name. On Linux, this will usually be
`/dev/ttyUSB0`, on Windows usually `COM3` and on Mac `/dev/cu.usbserial-xxxx` (where xxxx is
some number). On Mac and on Windows, you might need to install a serial interface driver, which
you can get from [https://www.silabs.com/developers/usb-to-uart-bridge-vcp-drivers](https://www.silabs.com/developers/usb-to-uart-bridge-vcp-drivers).
If you installed the Arduino IDE, that driver was most likely already installed for you.

Then run
these two commands, substituting the filename of the image you downloaded above:

    esptool.py --chip esp32 --port /dev/ttyUSB0 erase_flash
    esptool.py --chip esp32 --port /dev/ttyUSB0 --baud 460800 write_flash -z 0x1000 esp32spiram-idf4-20210202-v1.14.bin

Press the reset button once and your MicroPython should run now.

To determine whether this is the case, you can use the supplied `pyboard.py` file, which
will also help you copy the files to the device. Run this command:

    pyboard.py --device /dev/ttyUSB0 -c 'print("hello world")'

You should see `hello world` printed on your console. If that is the case, everything
is now set up correctly and running.

You can now use the commands above and in [scripts.txt] to enable the picture frame. If you
want to interact directly with the Micropython running on your controller, you need a serial
terminal. On Linux or Mac, you can use `screen`, which is usually built in, or any other
client like [minicom](https://en.wikipedia.org/wiki/Minicom) or
[picocom](https://github.com/npat-efault/picocom). On Windows, `putty` is the most convenient
terminal client. There are many more you can use, simply choose what is easiest for you.

Simply start your terminal in such a way, that it connects with the device you found above,
wait for it to connect and press Enter once. You should see the familiar `>>>` python prompt,
where you can immediately start typing commands.



## License

Copyright 2021 Johannes Spielmann  
This file is part of epd-pictureframe.

epd-pictureframe is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

epd-pictureframe is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with epd-pictureframe.  If not, see <https://www.gnu.org/licenses/>.

### 

Parts of the program are licensed under the terms of the MIT license. See
[the original Inkplate 6 libraries](https://github.com/e-radionicacom/Inkplate-6-micropython/)
for more information




# Based on:  Inkplate 6 micropython module

The libraries used by this project are pulled from [the original Inkplate 6 libraries](https://github.com/e-radionicacom/Inkplate-6-micropython/).

![](https://www.crowdsupply.com/img/040a/inkplate-6-angle-01_png_project-main.jpg)

Micropython for all-in-one e-paper display Inkplate 6 can be found in this repo. Inkplate 6 is a powerful, Wi-Fi enabled ESP32 based six-inch e-paper display – recycled from a Kindle e-reader. Its main feature is simplicity. Just plug in a USB cable, open Arduino IDE, and change the contents of the screen with few lines of code. Learn more about Inkplate 6 on [official website](https://inkplate.io/). Inkplate was crowdfunded on [Crowd Supply](https://www.crowdsupply.com/e-radionica/inkplate-6).

Original effort done by [tve](https://github.com/tve/micropython-inkplate6).

### Features

- Simple graphics class for monochrome use of the e-paper display
- Simple graphics class for 2 bits per pixel greyscale use of the e-paper display
- Support for partial updates (currently only on the monochrome display)
- Access to touch sensors
- Everything in pure python with screen updates virtually as fast as the Arduino C driver
- Bitmap drawing, although really slow one

### Getting started with micropython on Inkplate 6


- Flash MicroPython firmware supplied, or from http://micropython.org/download/esp32/ .
- Run 
  ```
  esptool.py --port /dev/ttyUSB0 erase_flash 
  ```
  to erase esp32 flash and then
  ```
  esptool.py --chip esp32 --port /dev/ttyUSB0 write_flash -z 0x1000 esp32spiram-idf4-20191220-v1.12.bin
  ```
  to flash supplied firmware.
  
  If you don't have esptool.py installed, install it from here: https://github.com/espressif/esptool.

- Copy library files to your board, something like:
  ```
  python3 pyboard.py --device /dev/ttyUSB0 -f cp mcp23017.py sdcard.py inkplate.py image.py gfx.py gfx_standard_font_01.py :
  ```
  (You can find `pyboard.py` in the MicroPython tools directory or just download it from
  GitHub: https://raw.githubusercontent.com/micropython/micropython/master/tools/pyboard.py)

- Run `example.py`:
  ```
  python3 pyboard.py --device /dev/ttyUSB0 example.py
  ```
- You can run our other 2 examples, showing how to use the Sd card and network class.

### Examples

The repo contains many examples which can demonstrate the Inkplate capabilites. 
- example.py -> demonstrates basic drawing capabilites, as well as drawing some images
- exampleNetwork.py -> demonstrates connection to WiFi network while drawing the HTTP request response on the screen
- exampleSd.py -> demonstrates reading files and images from SD card

### Battery power

Inkplate 6 has two options for powering it. First one is obvious - USB port at side of the board. Just plug any micro USB cable and you are good to go. Second option is battery. Supported batteries are standard Li-Ion/Li-Poly batteries with 3.7V nominal voltage. Connector for the battery is standard 2.00mm pitch JST connector. The onboard charger will charge the battery with 500mA when USB is plugged at the same time. You can use battery of any size or capacity if you don't have a enclosure. If you are using our enclosure, battery size shouldn't exceed 90mm x 40mm (3.5 x 1.57 inch) and 5mm (0.19 inch) in height. [This battery](https://e-radionica.com/en/li-ion-baterija-1200mah.html) is good fit for the Inkplate.

### Arduino?

Looking for Arduino library? Look [here](https://github.com/e-radionicacom/Inkplate-6-Arduino-library)!

### Open-source

All of Inkplate-related development is open-sourced:
- [Arduino library](https://github.com/e-radionicacom/Inkplate-6-Arduino-library)
- [Inkplate 6 hardware](https://github.com/e-radionicacom/Inkplate-6-hardware)
- [micropython Inkplate](https://github.com/e-radionicacom/Inkplate-6-micropython)
- [OSHWA certificate](https://certification.oshwa.org/hr000003.html)

### Where to buy & other

Inkplate 6 is available for purchase via:

- [e-radionica.com](https://e-radionica.com/en/inkplate.html)
- [Crowd Supply](https://www.crowdsupply.com/e-radionica/inkplate-6)
- [Mouser](https://hr.mouser.com/Search/Refine?Keyword=inkplate)
- [Sparkfun](https://www.sparkfun.com/search/results?term=inkplate)
- [Pimoroni](https://shop.pimoroni.com/products/inkplate-6)

Inkplate 6 is open-source. If you are looking for hardware design of the board, check the [Hardware repo](https://github.com/e-radionicacom/Inkplate-6-hardware). You will find 3D printable [enclosure](https://github.com/e-radionicacom/Inkplate-6-hardware/tree/master/3D%20printable%20case) there, as well as [detailed dimensions](https://github.com/e-radionicacom/Inkplate-6-hardware/tree/master/Technical%20drawings). In this repo you will find code for driving the ED060SC7 e-paper display used by Inkplate.

For all questions and issues, please use our [forum](http://forum.e-radionica.com/en) to ask an question.
For sales & collaboration, please reach us via [e-mail](mailto:kontakt@e-radionica.com).
