"""
Configuration for your display: change these settings to fit your environment and choices.

Copy this file to `config.py`, then change the values as appropriate. Then copy to your frame.
"""
# Copyright 2021 Johannes Spielmann
# This file is part of epd-pictureframe.
#
#     epd-pictureframe is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     epd-pictureframe is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with epd-pictureframe.  If not, see <https://www.gnu.org/licenses/>.

# Do you want to see debug output on the serial console?
debug = True

# the wifi name and password this display should connect to
ssid = ""
password = ""

# frame identifier
# we will use this to show analytics in the web-front-end
# use any name you like, eg. "living room-1" or "red in bedroom"
frame_id = ""

# set this to the correct URL for your image
image_url = ""

# delay settings
# during daytime, wait n milliseconds until next image
image_delay_day_ms = 60 * 60 * 1000  # once per hour
# during nighttime, we don't update as often
# note that there WILL be an update once daytime begins
image_delay_night_ms = 100 * 60 * 60 * 1000  # no updates at night
# when an error occurs, wait for this long
retry_delay_ms = 5 * 60 * 1000  # five minutes


# between these hours (UTC) the day mode is active,
# otherwise the night mode is active
day_start_hour = 6
day_end_hour = 19

# this is a good NTP host -- we will only use it once anyway, our RTC is quite good enough!
ntp_host = "time.fu-berlin.de"
