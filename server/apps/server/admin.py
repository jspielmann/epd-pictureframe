# Copyright 2021 Johannes Spielmann
# This file is part of epd-pictureframe.
#
#     epd-pictureframe is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     epd-pictureframe is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with epd-pictureframe.  If not, see <https://www.gnu.org/licenses/>.

from django.contrib import admin

from .models import Sequence, Image, FrameAnalytics


@admin.register(Sequence)
class Admin(admin.ModelAdmin):

    list_display = ("user", "title", "created", "seq_id")


@admin.register(Image)
class Admin(admin.ModelAdmin):

    list_display = ("id", "title", "sequence", "created")
    list_display_links = ("id", "title")


@admin.register(FrameAnalytics)
class Admin(admin.ModelAdmin):

    list_display = ("id", "frame_mac", "frame_id", "batt_level", "created")
    date_hierarchy = "created"
    list_filter = ("frame_mac", "frame_id", "created")
