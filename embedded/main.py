# Copyright 2021 Johannes Spielmann
# This file is part of epd-pictureframe.
#
#     epd-pictureframe is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     epd-pictureframe is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with epd-pictureframe.  If not, see <https://www.gnu.org/licenses/>.

import framebuf
import machine
import network
import ntptime
import ubinascii
import urequests
import time
from inkplate import Inkplate
import urlencode
import config
from machine import TouchPad, Pin
import esp32

# TODO: commands, esp. changing interval and sequence
# TODO: re-sync time, seems to drift quite a lot


TOUCH_PIN_RESET = 13
TOUCH_PIN_HALT = 14
TOUCH_THRESHOLD = 250   # rigorous scientific experimentation arrived at this value

_touch_reset = None
_touch_halt = None

def log(*args, **kwargs):
    if config.logging_enabled:
        print(*args, **kwargs)


def get_touchpad():
    global _touch_reset
    global _touch_halt
    if _touch_halt is None:
        _touch_halt = TouchPad(Pin(TOUCH_PIN_HALT))
    if _touch_reset is None:
        _touch_reset = TouchPad(Pin(TOUCH_PIN_RESET))

    return _touch_halt, _touch_reset


def check_touchpin():
    """Check the state of the GPIO13 touch pin."""

    touch_halt, touch_reset = get_touchpad()
    return touch_halt.read() < TOUCH_THRESHOLD


def do_connect():
    """Create a Wifi connection"""

    log("connecting")
    sta_if = network.WLAN(network.STA_IF)
    if not sta_if.isconnected():
        # print("connecting to network...")
        sta_if.active(True)
        sta_if.connect(config.ssid, config.password)
        while not sta_if.isconnected():
            pass

    log("network config:", sta_if.ifconfig())


def check_libraries():
    """Check that all necessary libraries are installed and (if not), install them via upip."""

    try:
        from collections.defaultdict import defaultdict
    except ImportError:
        import upip
        upip.install("collections")
        upip.install("collections.defaultdict")


def set_time():

    """Set time from NTP."""
    ntptime.host = config.ntp_host
    ntptime.settime()
    log("time is", time.localtime())


def get_image():
    """Get a new image from the image_url and show it on the display."""

    display = Inkplate(Inkplate.INKPLATE_2BIT)
    display.begin()

    log("getting image from ", config.image_url)
    frame_id = config.frame_id
    frame_mac = ubinascii.hexlify(network.WLAN().config("mac"), ":").decode()
    batt_level = display.readBattery()
    query = {"frame_id": frame_id, "frame_mac": frame_mac, "batt_level": batt_level}
    log(query)
    res = urequests.get(
        config.image_url + "?" + urlencode.urlencode(query),
    )
    data = res.content
    print("  received, loading...")
    data_buffer = bytearray(data)

    fb = framebuf.FrameBuffer(data_buffer, 800, 600, framebuf.GS2_HMSB)

    display.ipg.blit(fb, 0, 0)
    display.display()


def main():
    """Start image getting."""

    try:
        do_connect()
        check_libraries()
        set_time()
        get_image()
    except Exception as e:
        log(e)
        retry()
    go_to_sleep()


def main_wakeup():
    """Do what is necessary after waking up from deep sleep."""

    log("time is", time.localtime())
    try:
        do_connect()
        get_image()
    except Exception as e:
        log(e)
        retry()
    go_to_sleep()


def retry():
    """If any error occurs, we simply retry in ten minutes."""
    log("an error occurred, re-trying shortly")
    machine.deepsleep(config.retry_delay_ms)


def go_to_sleep():
    """Go to sleep again. Depending on time of day, we use a different sleep delay."""

    _, tp = get_touchpad()
    tp.config(TOUCH_THRESHOLD)
    esp32.wake_on_touch(True)

    (year, month, day, hour, minute, second, _, _) = time.localtime()

    def get_max_delay():
        if hour < config.day_start_hour:
            max_delay = config.day_start_hour * 60 * 60 * 1000 - (
                hour * 60 * 60 * 1000 + minute * 60 * 1000 + second * 1000
            )
        else:
            max_delay = (24 + config.day_start_hour) * 60 * 60 * 1000 - (
                hour * 60 * 60 * 1000 + minute * 60 * 1000 + second * 1000
            )
        return max_delay


    if config.day_start_hour < hour < config.day_end_hour:
        log("sleeping for", config.image_delay_day_ms)
        machine.deepsleep(config.image_delay_day_ms)
    else:
        # making sure we only sleep until daytime begins again!
        max_delay = get_max_delay()

        if max_delay < config.image_delay_night_ms:
            # we know that this is the start of a new day, so we're going to set our clock again
            set_time()
            max_delay = get_max_delay()
            log("sleeping for", max_delay)
            machine.deepsleep(max_delay)
        else:
            log("sleeping for", config.image_delay_night_ms)
            machine.deepsleep(config.image_delay_night_ms)


try:
    if __name__ == "__main__":
        if check_touchpin():
            print("stopping because of touched pin, reset to... uh... restart!")
            pass
        elif machine.reset_cause() == machine.DEEPSLEEP_RESET:
            log("waking up...")
            main_wakeup()
        else:
            log("starting...")
            main()
except Exception as e:
    # this is the hardest retry we can do: let's go for ten minutes
    log(e)
    machine.deepsleep(10 * 60 * 1000)
