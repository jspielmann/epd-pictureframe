# Copyright 2021 Johannes Spielmann
# This file is part of epd-pictureframe.
#
#     epd-pictureframe is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     epd-pictureframe is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with epd-pictureframe.  If not, see <https://www.gnu.org/licenses/>.

import random
from functools import lru_cache

from django.contrib.auth import get_user_model
from django.db import models
from django.urls import reverse
from django.utils import timezone


def get_new_sequence_id():
    alphabet = "0123456789abcdefghijklmnopqrstuvwxyz"
    new_id = "".join(random.choice(alphabet) for _ in range(32))
    if Sequence.objects.filter(seq_id=new_id).exists():
        return get_new_sequence_id()
    return new_id


class Sequence(models.Model):
    """An image sequence."""

    user = models.ForeignKey(get_user_model(), on_delete=models.CASCADE)
    seq_id = models.CharField(max_length=32, default=get_new_sequence_id)
    title = models.CharField(
        max_length=256, help_text="a name for your sequence, for easier identification", blank=True
    )

    image_width = models.PositiveIntegerField(default=800)
    image_height = models.PositiveIntegerField(default=600)

    created = models.DateTimeField(auto_now_add=True)

    def images(self):
        return self.image_set.all().order_by("-created")

    def get_absolute_url(self):
        return reverse("server:overview", args=(self.seq_id,))

    def __str__(self):
        return self.seq_id


def image_path(instance, filename):
    return f"image/{instance.sequence.seq_id}/{filename}"


def binary_path(instance, filename):
    return f"binary/{instance.sequence.seq_id}/{filename}"


class Image(models.Model):
    """A single image in a sequence."""

    sequence = models.ForeignKey(Sequence, on_delete=models.CASCADE)
    title = models.CharField(max_length=256, blank=True, help_text="(optional) image title, only shown here")

    original = models.ImageField(upload_to=image_path)

    cropped = models.ImageField(upload_to=image_path)
    binary = models.FileField(upload_to=binary_path)

    created = models.DateTimeField(auto_now_add=True)
    visible = models.BooleanField(
        default=False,
        help_text="Determine whether this image can be loaded onto a frame. For this to be true, we MUST have a binary blob and the user must have set this to True",
    )

    def get_title(self):
        if self.title:
            return self.title
        return str(self.id)

    @lru_cache(1)
    def get_frames(self):
        """Return a list of names of frames that currently show this image (ie. was last sent to those frames)."""

        analytics = FrameAnalytics.objects.filter(sequence=self.sequence)
        frame_macs = analytics.values_list("frame_mac").distinct()

        frames = []
        for frame_mac in frame_macs:
            select_last = (
                FrameAnalytics.objects.filter(sequence=self.sequence, frame_mac=frame_mac[0], image=self)
                .order_by("-created")
                .first()
            )
            if select_last:
                frames.append(select_last.frame_id)

        return frames

    def strength(self):
        """Compute the image selection strength for this image."""
        age = timezone.now() - self.created
        strength = 100 - age.days if age.days <= 100 else 1
        return strength

    def get_absolute_url(self):
        return reverse("server:image_detail", args=(self.sequence.seq_id, self.id))


class FrameAnalytics(models.Model):
    """Data about frame requests and the stuff they sent with."""

    sequence = models.ForeignKey(Sequence, on_delete=models.CASCADE)
    image = models.ForeignKey(Image, on_delete=models.SET_NULL, null=True, blank=True)

    frame_mac = models.CharField(max_length=32, null=True, blank=True)
    frame_id = models.CharField(max_length=128, null=True, blank=True)
    batt_level = models.FloatField(null=True, blank=True)

    created = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name = "Frame analytics"
        verbose_name_plural = "Frame analytics"
