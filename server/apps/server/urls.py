# Copyright 2021 Johannes Spielmann
# This file is part of epd-pictureframe.
#
#     epd-pictureframe is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     epd-pictureframe is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with epd-pictureframe.  If not, see <https://www.gnu.org/licenses/>.

from django.urls import path, register_converter

from . import views

app_name = "server"


class SequenceIdConverter:
    regex = "[0-9a-z]{32}"

    def to_python(self, value):
        return value

    def to_url(self, value):
        return value


register_converter(SequenceIdConverter, "id")


urlpatterns = [
    path("", views.Root, name="root"),
    path("add/", views.add_sequence, name="add_sequence"),
    path("<id:id>/overview/", views.sequence, name="overview"),
    path("<id:id>/overview/<int:image_id>/", views.image_detail, name="image_detail"),
    path("<id:id>/overview/<int:pk>/edit/", views.ImageEdit, name="image_edit"),
    path("<id:id>/overview/<int:image_id>/delete/", views.image_delete, name="image_delete"),
    path("<id:id>/overview/<int:image_id>/activate/", views.image_activate, name="image_activate"),
    path("<id:id>/overview/<int:image_id>/deactivate/", views.image_deactivate, name="image_deactivate"),
    path("<id:id>/analytics/", views.analytics, name="analytics"),
    path("<id:id>/analytics/<int:frame_analytics_id>/", views.analytics_detail, name="analytics_detail"),
    path("<id:id>/add/", views.add_image, name="image_add"),
    # this is the URL you'll want to retrieve for your image
    path("<id:id>/", views.image, name="image"),
]
