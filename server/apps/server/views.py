# Copyright 2021 Johannes Spielmann
# This file is part of epd-pictureframe.
#
#     epd-pictureframe is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     epd-pictureframe is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with epd-pictureframe.  If not, see <https://www.gnu.org/licenses/>.

import os
import random
from io import BytesIO

import base64

import PIL
import hitherdither
from PIL import ImageOps
from django.conf import settings
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.core.files.base import ContentFile
from django.core.files.images import ImageFile
from django.http import FileResponse, HttpResponseBadRequest
from django.shortcuts import render, get_object_or_404, redirect
from django.urls import reverse
from django.utils import timezone
from django.views import View
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import ListView, CreateView, DeleteView, DetailView

from .models import Sequence, Image, FrameAnalytics


def as_view(cls):
    return cls.as_view()


@login_required
@as_view
class Root(ListView):
    template_name = "server/root.djhtml"

    def get_queryset(self):
        return Sequence.objects.filter(user=self.request.user)


@login_required
@as_view
class add_sequence(CreateView):
    template_name = "server/add_sequence.djhtml"
    model = Sequence
    fields = ("title", "image_width", "image_height")

    def form_valid(self, form):
        self.object = object = form.save(commit=False)
        object.user = self.request.user
        object.save()
        return super().form_valid(form)


@as_view
class sequence(ListView):

    template_name = "server/sequence.djhtml"
    paginate_by = 20

    def get_queryset(self):
        self.sequence = sequence = get_object_or_404(Sequence, seq_id=self.kwargs.get("id"))
        return sequence.images()

    def get_context_data(self, *, object_list=None, **kwargs):
        ctx = super().get_context_data(object_list=object_list, **kwargs)

        ctx["sequence"] = self.sequence
        ctx["sequence_url"] = self.request.build_absolute_uri(reverse("server:image", args=(self.sequence.seq_id,)))

        return ctx


def image_detail(request, id, image_id):
    """One image in a sequence"""

    image = get_object_or_404(Image, sequence__seq_id=id, id=image_id)

    return render(
        request,
        "server/image_detail.djhtml",
        {
            "image": image,
        },
    )


@as_view
class image_delete(DeleteView):

    template_name = "server/image_delete.djhtml"

    def get_object(self, queryset=None):
        return get_object_or_404(
            Image,
            sequence__seq_id=self.kwargs.get("id"),
            id=self.kwargs.get("image_id"),
        )

    def get_success_url(self):
        return reverse("server:overview", args=(self.object.sequence.seq_id,))


@as_view
class image_activate(View):
    def get(self, request, *args, **kwargs):
        image = get_object_or_404(
            Image,
            sequence__seq_id=self.kwargs.get("id"),
            id=self.kwargs.get("image_id"),
        )
        image.visible = True
        image.save()

        messages.success(request, f'Image "{image.get_title()}" will be considered for sending to frames.')

        return redirect("server:overview", id=kwargs.get("id"))


@as_view
class image_deactivate(View):
    def get(self, request, *args, **kwargs):
        image = get_object_or_404(
            Image,
            sequence__seq_id=self.kwargs.get("id"),
            id=self.kwargs.get("image_id"),
        )
        image.visible = False
        image.save()

        if image.title:
            title = image.title
        else:
            title = image.id

        messages.success(request, f'Image "{image.get_title()}" will not be sent to frames any more.')

        return redirect("server:overview", id=kwargs.get("id"))


def analytics(request, id):
    """Show frame analytics for a given sequence."""

    sequence = get_object_or_404(Sequence, seq_id=id)

    analytics = FrameAnalytics.objects.filter(sequence=sequence)
    frame_macs = analytics.values_list("frame_mac").distinct()

    frame_last = []
    for frame_mac in frame_macs:
        select_last = FrameAnalytics.objects.filter(sequence=sequence, frame_mac=frame_mac[0]).order_by("-created")
        if select_last:
            frame_last.append(select_last[0])

    return render(
        request,
        "server/analytics.djhtml",
        {
            "sequence": sequence,
            "frames": frame_last,
        },
    )


def analytics_detail(request, id, frame_analytics_id):
    """Show frame analytics for a given sequence."""

    sequence = get_object_or_404(Sequence, seq_id=id)

    analytics = FrameAnalytics.objects.filter(sequence=sequence)
    frame_analytics_entry = get_object_or_404(analytics, id=frame_analytics_id)

    frame_data = analytics.filter(frame_mac=frame_analytics_entry.frame_mac).order_by("-created")

    return render(
        request,
        "server/analytics_detail.djhtml",
        {
            "sequence": sequence,
            "frames": frame_data,
            "frame": frame_data[0],
        },
    )


@csrf_exempt
@login_required
@as_view
class add_image(CreateView):
    template_name = "server/add_image.djhtml"
    model = Image
    fields = ("title", "original")

    def get_success_url(self):
        return reverse("server:image_edit", args=(self.object.sequence.seq_id, self.object.id))

    def form_valid(self, form):
        self.object = object = form.save(commit=False)
        object.sequence = get_object_or_404(Sequence, seq_id=self.kwargs["id"])
        object.save()

        return super().form_valid(form)


@csrf_exempt
@as_view
class ImageEdit(DetailView):

    template_name = "server/image_edit.djhtml"

    def get_queryset(self):
        return Image.objects.filter(sequence__seq_id=self.kwargs["id"])

    def post(self, request, *args, **kwargs):

        image = self.get_object()

        image.visible = request.POST.get("active", "false") == "true"

        image_data_b64 = request.POST.get("image_data")

        if not image_data_b64.startswith("data:image/png;base64,"):
            return HttpResponseBadRequest()

        image_data_b64 = image_data_b64[22:]
        image_data = base64.b64decode(image_data_b64)
        buff = BytesIO(image_data)
        img = PIL.Image.open(buff)

        buffer = BytesIO()
        img.convert("RGB").save(fp=buffer, format="jpeg")
        image.cropped = ImageFile(buffer, name=f"cropped_{image.id}.jpg")

        # create packed as binary blob for the frame
        b = bytearray(800 * 600)

        px = img.convert("L").load()
        for x in range(800):
            for y in range(600):
                try:
                    b[y * 800 + x] = px[x, y]
                except IndexError as e:
                    print(x, y, e)
                    raise e

        packed_b = bytearray(800 * 600 // 4)
        for i in range(len(packed_b)):
            grp = b[4 * i : 4 * i + 4]
            single = (grp[3] & 3) << 6 | (grp[2] & 3) << 4 | (grp[1] & 3) << 2 | (grp[0] & 3)
            packed_b[i] = single

        # save to binary field
        binary_file = ContentFile(packed_b)
        try:
            os.unlink(image.binary.path)
        except Exception:
            pass
        image.binary.save(f"{image.id}.bin", binary_file)

        image.save()

        return self.get(request, *args, **kwargs)

    def dither_and_pack(self, object):
        # TODO: this function is old!
        # TODO: use sequence set-up for image cropping/scaling/dithering/packing

        # crop to ratio
        im = PIL.Image.open(object.original)
        width, height = im.size
        if width / height > 800 / 600:
            # height limit, crop to height
            crop_height = height
            crop_width = height * 1.333333333333333333
        else:
            # width limit
            crop_width = width
            crop_height = width / 1.333333333333333333

        crop_width, crop_height = round(crop_width), round(crop_height)
        im = im.crop((0, 0, crop_width, crop_height))

        # resize and grayscale (with RGB backing for the stupid dithering)
        im = im.resize((800, 600))
        gray_im = ImageOps.grayscale(im)
        im = PIL.Image.new("RGB", gray_im.size)
        im.paste(gray_im)

        # dither
        palette = hitherdither.palette.Palette.create_by_median_cut(im, n=4)
        img_dithered = hitherdither.diffusion.error_diffusion_dithering(
            im, palette, method="jarvis-judice-ninke", order=3
        )

        # save cropped image
        buffer = BytesIO()
        img_dithered.convert("RGB").save(fp=buffer, format="jpeg")
        object.cropped = ImageFile(buffer, name=f"cropped_{object.id}.jpg")

        # create packed as binary blob for the frame
        b = bytearray(800 * 600)

        px = img_dithered.load()
        for x in range(800):
            for y in range(600):
                try:
                    b[y * 800 + x] = px[x, y]
                except IndexError as e:
                    print(x, y, e)
                    raise e

        packed_b = bytearray(800 * 600 // 4)
        for i in range(len(packed_b)):
            grp = b[4 * i : 4 * i + 4]
            single = (grp[3] & 3) << 6 | (grp[2] & 3) << 4 | (grp[1] & 3) << 2 | (grp[0] & 3)
            packed_b[i] = single

        # save to binary field
        binary_file = ContentFile(packed_b)
        object.binary.save(f"{object.id}.bin", binary_file)

        object.save()


def image(request, id):
    """Server a random image from this sequence."""

    sequence = get_object_or_404(Sequence, seq_id=id)

    # store frame information for analytics
    frame_id = request.GET.get("frame_id")
    frame_mac = request.GET.get("frame_mac")
    batt_level = request.GET.get("batt_level")
    fa = FrameAnalytics.objects.create(sequence=sequence, frame_id=frame_id, frame_mac=frame_mac, batt_level=batt_level)

    images = list(
        Image.objects.filter(sequence_id=sequence.id, visible=True)
        .order_by("-created")
        .values_list("binary", "created", "id")
    )

    selection_list = []
    selection_max = 0
    now = timezone.now()
    for binary, dt, image_id in images:
        age = now - dt
        strength = 100 - age.days if age.days <= 100 else 1
        selection_max += strength
        selection_list.append(selection_max)

    selection_point = random.randrange(selection_max)
    for i in range(len(selection_list)):
        if selection_list[i] > selection_point:
            break

    selected = images[i][0]
    fa.image_id = images[i][2]
    fa.save()

    abs_path = os.path.join(settings.MEDIA_ROOT, selected)
    f = open(abs_path, "rb")
    return FileResponse(f)
